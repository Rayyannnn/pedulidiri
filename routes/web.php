<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\userController;
use App\Http\Controllers\perjalananController;
use App\Http\Controllers\logincontroller;
use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',[logincontroller::class,'halamanlogin'])->name('login');
Route::any('/postlogin',[logincontroller::class,'login']);
Route::get('/register',[userController::class,'halamanRegister']);
Route::post('/simpanUser',[userController::class,'simpanRegister']);
Route::get('/input',[perjalananController::class,'halamanInput']);
Route::get('/dashboard',[perjalananController::class,'index']);
Route::post('/simpanPerjalanan',[perjalananController::class,'savePerjalanan']);
Route::get('/cari', [DashboardController::class, 'cariPerjalanan']);
Route::get('/sort', [perjalananController::class, 'sort']);
Route::get('/home', function () {
    return view('pages.place');
});
Route::get('/about', function () {
    return view('pages.about');
});
Route::get('/logout',[logincontroller::class,'logOut']);

