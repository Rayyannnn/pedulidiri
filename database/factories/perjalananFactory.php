<?php

namespace Database\Factories;

use App\Models\perjalanan;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class perjalananFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = perjalanan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_user' => 1,
            'tanggal' => $this->faker->dateTimeBetween('-2 year', 'now'),
            'lokasi' => $this->faker->streetAddress(),
            'suhu' => $this->faker->numberBetween(32, 36),
            'jam' =>$this->faker->time()
        ];
    }
}
