<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller{

    
public function cariPerjalanan(Request $request){
    if(!empty($request->input('lokasi'))&&empty($request->input('suhu'))&&empty($request->input('tanggal'))&&empty($request->input('jam'))){
        $cari=$request->lokasi;
        $data = User::join('perjalanans','perjalanans.id_user','=','users.id')
        ->where(function($query)use($cari){
            $query->where('users.nama',auth()->user()->nama)
            ->where('perjalanans.lokasi','LIKE','%'.$cari.'%');
        })->get(['users.nama','perjalanans.*']);
        if ($data) {
            return view('pages.table',['data'=>$data])->with('alert','data di temukan');
        }else{
            abort(404);
        }
    }elseif(empty($request->input('lokasi'))&& !empty($request->input('suhu'))&&empty($request->input('tanggal'))&&empty($request->input('jam'))){
        $cari=$request->suhu;
        $data = User::join('perjalanans','perjalanans.id_user','=','users.id')
        ->where(function($query)use($cari){
            $query->where('perjalanans.suhu','LIKE','%'.$cari.'%');
        })->get(['users.nama','perjalanans.*']);
        if ($data) {
            return view('pages.table',['data'=>$data])->with('alert','data di temukan');
        }else{
            abort(404);
        }
    }elseif(empty($request->input('lokasi'))&& empty($request->input('suhu'))&& !empty($request->input('tanggal'))&&empty($request->input('jam'))){
    $cari=$request->tanggal;
    $data = User::join('perjalanans','perjalanans.id_user','=','users.id')
    ->where(function($query)use($cari){
        $query->where('perjalanans.tanggal','LIKE','%'.$cari.'%');
    })->get(['users.nama','perjalanans.*']);
    if ($data) {
        return view('pages.table',['data'=>$data])->with('alert','data di temukan');
    }else{
        abort(404);
    }

    }elseif(empty($request->input('lokasi'))&& empty($request->input('suhu'))&& empty($request->input('tanggal'))&& !empty($request->input('jam'))){
        $cari=$request->jam;
        $data = User::join('perjalanans','perjalanans.id_user','=','users.id')
        ->where(function($query)use($cari){
            $query->where('perjalanans.jam','LIKE','%'.$cari.'%');
        })->get(['users.nama','perjalanans.*']);
        if ($data) {
            return view('pages.table',['data'=>$data])->with('alert','data di temukan');
        }else{
            abort(404);
        }
    }else {
        $data = DB::table('perjalanans')
        ->join('users', 'users.id', '=', 'perjalanans.id_user')
        ->select('users.email','perjalanans.tanggal','perjalanans.jam', 'perjalanans.lokasi', 'perjalanans.suhu')
        ->where('users.nama','=',auth()->user()->nama)
        ->get();
        return view('pages.table',['data'=>$data]);
    }
}

}