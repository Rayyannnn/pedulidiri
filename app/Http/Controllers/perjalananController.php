<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\perjalanan;
use Illuminate\Support\Facades\DB;

class perjalananController extends Controller
{
    public function index(){

        if(is_null(auth()->user())){
            return redirect()->route('login');
        }

        $data = DB::table('perjalanans')
        ->join('users', 'users.id', '=', 'perjalanans.id_user')
        ->select('users.email','perjalanans.tanggal','perjalanans.jam', 'perjalanans.lokasi', 'perjalanans.suhu')
        ->where('nama','=',auth()->user()-> nama)
        ->simplePaginate(7);
        return view('pages.table',['data'=>$data]);
    }

    public function halamanInput(){
        return view('pages.input');
    }

    public function savePerjalanan(Request $request){

        $data=[
            'id_user'=>auth()->user()->id,
            'tanggal'=>$request->tanggal,
            'jam'=>$request->jam,
            'lokasi'=>$request->lokasi,
            'suhu'=>$request->suhu
        ];

        perjalanan::create($data);
        return redirect('/dashboard')->with('success', 'Data telah ditambahkan');  
    }
    

    

    public function sort (Request $request){
        {
            $data = perjalanan ::all()->where('id_user','=',auth()->user()->id);

            

            if($request -> input('suhuDesc')){
                $sort = $request->suhu;
                $sorted = $data->SortByDesc('suhu');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }else if($request->input('suhuAsc')){
                $urut = $request->suhu;
                $sorted = $data->SortBy('suhu');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }

            if($request -> input('tanggalDesc')){
                $urut = $request->tanggal;
                $sorted = $data->SortByDesc('tanggal');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }else if($request->input('tanggalAsc')){
                $urut = $request->tanggal;
                $sorted = $data->SortBy('tanggal');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }

            if($request -> input('jamDesc')){
                $urut = $request->jam;
                $sorted = $data->SortByDesc('jam');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }else if($request->input('jamAsc')){
                $urut = $request->jam;
                $sorted = $data->SortBy('jam');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }

            if($request -> input('lokasiDesc')){
                $urut = $request->lokasi;
                $sorted = $data->SortByDesc('lokasi');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }else if($request->input('lokasiAsc')){
                $urut = $request->lokasi;
                $sorted = $data->SortBy('lokasi');
                return view('pages.table',['data'=>$sorted->values()->all()]);
            }

        }
    } 
}
