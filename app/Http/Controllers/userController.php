<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class userController extends Controller
{

    public function halamanRegister(){
        return view('pages.register');
    }
    

    public function simpanRegister(Request $request){
        $request->validate([
            'nik'=>'required|unique:users,email',
            'nama'=>'required'
        ],
        [
            'nik.unique'=>'NIK Sudah Terdaftar',
            'nama.required'=>'nama tidak boleh kosong'
        ]);

        $data=[
            'nama'=>$request->nama,
            'email'=>$request->nik,
            'password'=>bcrypt($request->nik)
        ];

        //dd($data);
        

        User::create($data);
        return redirect('/login')->with('message',"registrasi berhasil");
    }
}
