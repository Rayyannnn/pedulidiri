<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;

class logincontroller extends Controller
{
    public function __construct(){
        $this->middleware('guest')->except('logOut');
    }
    public function halamanlogin(){
        return view('pages.login');
    }
    public function login(Request $request){

        if(Auth::attempt($request->only('nama','email','password'))){
            return redirect('/home');
        } 
        return redirect('/login')->with('message','login gagal');
    }
        public function logOut(){
            Auth::logout();
            return redirect('/login')->with('logoutSuccess', 'Anda telah berhasil logout');
        }

    
}
