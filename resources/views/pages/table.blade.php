@extends('layout.dashboard')

<title>PD &mdash; Data User</title>

@section('body')
<style>
  @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Koulen&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');


  body{
    background:url(../assets/img/wave.svg) ;
  }

  .kopi{
    font-size: 25px;
    color: white;
    margin-bottom: 10px;
    font-family: 'Koulen', cursive;
    
  }
</style>
  <br>
  <br>
  <br>
  <br>
@php
$no = 1;
@endphp
<p class="kopi">Data User</p>
<div class="col-12">
      <div class="card" style="background: #F7F4E9">
          <div class="card-body">
            <form class="form-inline mr-auto" method="GET" action="/cari">
              @csrf
              <div class="col-auto">
                <select onchange="yesnoCheck(this);" class="form-control form-select" type="search">
                <option value="lokasi">lokasi</option>
                <option value="tanggal">Taggal</option>
                <option value="jam">Jam</option>
                <option value="suhu">Suhu</option>
                </select>
              </div>
          
              <div class="col">
                <div class="form-group">
                  <input name="lokasi" id="iflokasi" class="form-control" type="string" placeholder="Cari lokasi" aria-label="Search"> 
                  <button id="ifBtnlokasi" style="display: none" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button> 
                </div>
                
                 <div class="form-group">
                  <input name="tanggal" id="iftgl" style="display:none;" class="form-control" type="date" placeholder="Cari Tanggal" aria-label="Search">
                  <button id="ifBtntgl" style="display: none" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button> 
                </div>
          
                <div class="form-group">
                  <input name="jam" id="ifjam" style="display:none;" class="form-control" type="time" placeholder="Cari Jam" aria-label="Search">
                  <button id="ifBtnjam" style="display: none;" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button> 
                </div>
          
                <div class="form-group">
                  <input name="suhu" id="ifsuhu" style="display:none;" class="form-control" type="integer" placeholder="Cari Suhu" aria-label="Search">
                  <button id="ifBtnsuhu" style="display: none;" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button> 
                </div>
              </div>
            </form>
              <div class="table-responsive">
                <table class="table">
                  <thead class="thead-dark">
                          <tr>
                              <th scope="col">No</th>
                              <th scope="col">Tanggal <div class="btn-group" role="group">
                                  <button type="button" class="btn dropdown-toggle float-right text-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  </button>
                                  <form action="/sort" method="GET">
                                @csrf
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop4">
                                    <button class="dropdown-item" name="tanggalDesc" value="Desc" href = "/sort"> Terbaru </button>
                                    <button class="dropdown-item" name="tanggalAsc" value="Asc" href = "/sort"> Terlama</button>
                                </div>
                                </form>
                              </div></th>


                              <th scope="col">Jam <div class="btn-group" role="group">
                                <button type="button" class="btn dropdown-toggle float-right text-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <form action="/sort" method="GET">
                              @csrf
                              <div class="dropdown-menu" aria-labelledby="btnGroupDrop4">
                                  <button class="dropdown-item" name="jamDesc" value="Desc" href = "/sort"> Terbaru </button>
                                  <button class="dropdown-item" name="jamAsc" value="Asc" href = "/sort"> Terlama</button>
                              </div>
                              </form>
                            </div></th>

                              <th scope="col">Lokasi <div class="btn-group" role="group">
                                <button type="button" class="btn dropdown-toggle float-right text-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <form action="/sort" method="GET">
                              @csrf
                              <div class="dropdown-menu" aria-labelledby="btnGroupDrop4">
                                  <button class="dropdown-item" name="lokasiDesc" value="Desc" href = "/sort"> Z-A </button>
                                  <button class="dropdown-item" name="lokasiAsc" value="Asc" href = "/sort"> A-Z </button>
                              </div>
                              </form>
                            </div></th>

                              <th scope="col">Suhu <div class="btn-group" role="group">
                                <button type="button" class="btn dropdown-toggle float-right text-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <form action="/sort" method="GET">
                              @csrf
                              <div class="dropdown-menu" aria-labelledby="btnGroupDrop4">
                                  <button class="dropdown-item" name="lokasiDesc" value="Desc" href = "/sort"> Tertinggi </button>
                                  <button class="dropdown-item" name="lokasiAsc" value="Asc" href = "/sort"> Terendah</button>
                              </div>
                              </form>
                            </div></th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach ($data as $perjalanans)
                              <tr>
                                  <th scope="row">{{ $no++ }}</th>
                                  <td>{{ $perjalanans->tanggal }}</td>
                                  <td>{{ $perjalanans->jam }}</td>
                                  <td>{{ $perjalanans->lokasi }}</td>
                                  <td>{{ $perjalanans->suhu }}</td>
                              </tr>
                          @endforeach
                      </tbody>
                </table>

                <div class="d-flex flex-row-reverse">
                  <button  type="button" class="btn" style="background-color: #5F9EA0" ><a href="/dashboard" style="color: white" ><strong>Refresh Data</strong></a>
                  </button>
                </div>
              </div>
          </div>
      </div>
  </div>
</div>

<script>
  function yesnoCheck(that) {
    if (that.value == "tanggal") {
      document.getElementById("iftgl").style.display = "block";
      document.getElementById("ifBtntgl").style.display = "block";

      document.getElementById("iflokasi").style.display = "none";
      document.getElementById("ifBtnlokasi").style.display = "none";

      document.getElementById("ifjam").style.display = "none";
      document.getElementById("ifBtnjam").style.display = "none";

      document.getElementById("ifsuhu").style.display = "none";
      document.getElementById("ifBtnsuhu").style.display = "none";

    }else if (that.value == "jam") {
      document.getElementById("iftgl").style.display = "none";
      document.getElementById("ifBtntgl").style.display = "none";

      document.getElementById("iflokasi").style.display = "none";
      document.getElementById("ifBtnlokasi").style.display = "none";

      document.getElementById("ifjam").style.display = "block";
      document.getElementById("ifBtnjam").style.display = "block";

      document.getElementById("ifsuhu").style.display = "none";
      document.getElementById("ifBtnsuhu").style.display = "none";
    
    } else  if(that.value == "suhu"){
      document.getElementById("iftgl").style.display = "none";
      document.getElementById("ifBtntgl").style.display = "none";

      document.getElementById("iflokasi").style.display = "none";
      document.getElementById("ifBtnlokasi").style.display = "none";

      document.getElementById("ifjam").style.display = "none";
      document.getElementById("ifBtnjam").style.display = "none";

      document.getElementById("ifsuhu").style.display = "block";
      document.getElementById("ifBtnsuhu").style.display = "block";
      
    }else {
      document.getElementById("iftgl").style.display = "none";
      document.getElementById("ifBtntgl").style.display = "none";

      document.getElementById("iflokasi").style.display = "block";
      document.getElementById("ifBtnlokasi").style.display = "block";

      document.getElementById("ifjam").style.display = "none";
      document.getElementById("ifBtnjam").style.display = "none";

      document.getElementById("ifsuhu").style.display = "none";
      document.getElementById("ifBtnsuhu").style.display = "none";
    }
    
  }
</script>

@endsection