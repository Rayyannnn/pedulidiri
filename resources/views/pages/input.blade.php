@extends('layout.dashboard')

@section('halaman')
    <h1>Riwayat Perjalanan</h1>
@endsection

@section('judul_card')
    Data Perjalanan
@endsection

<title>PD &mdash; Input Data</title>

@section('body')
<style> 
  @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Koulen&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');
.kopi{
  font-size: 25px;
  color: white;
  margin-bottom: 10px;
  font-family: 'Koulen', cursive;
}</style>
<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-12">
    <p class="kopi">Input Data</p>
      <div class="card" style="background: #F7F4E9">
          <div class="card-body">
            <form method="POST" action="/simpanPerjalanan">
              {{ csrf_field() }}
      
              <div class="form-group">
                <label for="tanggal">Tanggal</label>
                <input id="tanggal" type="date" class="form-control bg-secondary" name="tanggal" required>
              </div>
      
              <div class="form-group">
                <label for="jam">Jam</label>
                <input id="jam" type="time" class="form-control bg-secondary" name="jam" required>
              </div>
      
              <div class="form-group">
                  <label for="lokasi">Lokasi</label>
                  <input id="lokasi" type="string" class="form-control bg-secondary" name="lokasi" required >
              </div>
      
              <div class="form-group">
                <label for="suhu">Suhu</label>
                <input id="suhu" type="integer" class="form-control bg-secondary" name="suhu" required>
              </div>
              <div class="form-group">
                <div class="d-flex flex-row-reverse">
                <button type="submit" class="btn btn-lg btn-block col-2" style="background-color: #5F9EA0;">
                 <a style="color: white" ><strong>Input</strong></a>
                </button>
                </div>
              </div>
            </form>
          </div>
      </div>
  </div>
</div>
@endsection