

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PD &mdash; Login</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/style.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/components.css">
</head>

<body>

    <style>
        body{
            background:#E5E4E2;
        }

.login-dark form {
  width:500px;
  background-color:#1e2833;
  padding:40px;
  border-radius:4px;
  transform:translate(-50%, -50%);
  position:absolute;
  top:50%;
  left:50%;
  color:#fff;
  box-shadow:3px 3px 4px rgba(0,0,0,0.2);
}

.login-dark .illustration {
  text-align:center;
  padding:15px 0 20px;
  font-size:100px;
  color:#2980ef;
}

.login-dark form .form-control {
  background:none;
  border:none;
  border-bottom:1px solid #434a52;
  border-radius:0;
  box-shadow:none;
  outline:none;
  color:inherit;
}

.login-dark form .btn-primary {
  background:#214a80;
  border:none;
  border-radius:4px;
  padding:11px;
  box-shadow:none;
  margin-top:26px;
  text-shadow:none;
  outline:none;
}

.login-dark form .btn-primary:hover, .login-dark form .btn-primary:active {
  background:#214a80;
  outline:none;
}

.login-dark form .forgot {
  display:block;
  text-align:center;
  font-size:12px;
  color:#6f7a85;
  opacity:0.9;
  text-decoration:none;
}

.login-dark form .forgot:hover, .login-dark form .forgot:active {
  opacity:1;
  text-decoration:none;
}

.login-dark form .btn-primary:active {
  transform:translateY(1px);
}

@import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Koulen&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');

    </style>
    <div class="login-dark">
        <form method="POST" action="/postlogin" class="needs-validation" novalidate="">
            {{ csrf_field() }}
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group"></div>

            <div class="form-group"> 
                <input id="email" type="text" class="form-control" name="email" placeholder="NIK" tabindex="1" required autofocus>
                <input id="password" type="hidden" class="form-control" name="password" tabindex="1" required autofocus>
            </div>


            <div class="form-group">
                <input id="nama" type="text" class="form-control" name="nama" placeholder="Nama Lengkap" tabindex="1" required >
            </div>

            <script>
                window.onload=function(){
                  var src = document.getElementById('email'),
                      dst = document.getElementById('password');
                  src.addEventListener('input',function(){
                    dst.value = src.value;
                  });
                };
              </script>

            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit" tabindex="4" >Log In</button>
            </div>
        
            <a href="/register" class="forgot">Create Account</a></form>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    </div>

</body>

</html>
