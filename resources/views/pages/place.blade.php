@extends('layout.dashboard')

<title>PD &mdash; Home</title>

@section('body')
<style>
   @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');
   @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');
   @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');
   @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@100&family=Josefin+Sans:wght@200&family=Karla&family=Koulen&family=Nunito:wght@300&family=Open+Sans:wght@300&family=Prompt:wght@500&family=Red+Hat+Mono:wght@300&family=Roboto+Serif:opsz,wght@8..144,500&family=Roboto:wght@900&family=Smooch+Sans:wght@200&family=Vazirmatn:wght@100&display=swap');
    body{
        color: white;
    }

    .hofy{
        font-size: 30px;
        font-family: 'Prompt', sans-serif;
    }

    .holy{
        font-size: 40px;
        font-family: 'Nunito', sans-serif;
    }

    .pk{
        font-style: inherit;
        font-size: 15px;
        font-family: 'Nunito', sans-serif;
          }

    .jog{
        height: 100px;
        margin-bottom: 50px;
        margin-right: 30px;
    }

    .himb{
        font-size: 20px;
        font-family: 'Karla', sans-serif;

    }

    .joox{
        font-size: 20px;
        margin-bottom: 100px;
        color: goldenrod;
    }
</style>
<br>
<br>
<br>
<br>

<div class="d-flex justify-content-center">
  <p class="hofy"><strong>Haii            @if (!empty(auth()->user()->nama))
    {{ auth()->user()->nama }}   
    @else
     user 
    @endif,Mau kemana anda hari ini?</strong></p>
</div>
<br>
<br>
<hr>

<div class="container-fluid bg-3 text-center">    
    <div class="row">
      <div class="col-sm-3">
        <img src="../assets/img/place/semarang.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Semarang</strong></p>
      </div>
      <div class="col-sm-3"> 
        <img src="../assets/img/place/palembang.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Palembang</strong></p>
      </div>
      <div class="col-sm-3"> 
        <img src="../assets/img/place/bandung.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Bandung</strong></p>
      </div>
      <div class="col-sm-3">
        <img src="../assets/img/place/bukittinggi.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Bukittinggi</strong></p>
      </div>
      <div class="col-sm-3">
        <img src="../assets/img/place/aceh.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Banda Aceh</strong></p>
      </div>
      <div class="col-sm-3"> 
        <img src="../assets/img/place/surabaya.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Surabaya</strong></p>
      </div>
      <div class="col-sm-3"> 
        <img src="../assets/img/place/tanggerang.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Tanggerang</strong></p>
      </div>
      <div class="col-sm-3">
        <img src="../assets/img/place/denpasar.jpg" class="img-responsive" style="width:100%" alt="Image">
        <p class="pk"><strong>Denpasar</strong></p>
      </div>
    </div>
</div>
<br><br>
<div class="d-flex justify-content-center">
    <button type="button" class="btn btn-warning"><a href="#" style="color: black" ><strong>Show More</strong></a></button>
</div>
<br><br><br><br><br>


<div class="d-flex flex-row ">
    <p class="holy"><strong>Apa aja sih yang harus diperhatikan?</strong></p>
</div><br><br><br>
<div id="container" style="white-space:nowrap">
    <div id="image" style="display:inline;">
        <img class="jog" src="../assets/img/icon/injection.png">
    </div>
    <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
      <strong> Pastikan anda sudah vaksin 2x yaa,agar anda aman dari virus virus berbahaya.</strong>
    </div>
</div>
<div id="container" style="white-space:nowrap">
    <div id="image" style="display:inline;">
        <img class="jog" src="../assets/img/icon/virus.png">
    </div>
    <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
      <strong> Tidak terjangkit virus dalam kurun waktu 14 hari kebelakang.</strong>
    </div>
</div>
<div id="container" style="white-space:nowrap">
  <div id="image" style="display:inline;">
      <img class="jog" src="../assets/img/icon/healthcare.png">
  </div>
  <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
     <strong> Anda juga harus dalam keaadaan sehat yaa </strong>
  </div>
</div><br><br><br>
    <div class="container-fluid">
        <div class="d-flex justify-content-center">
         <p class="joox">Sudah memenuhi syarat diatas?ayo input data perjalananmu terlebih dahulu <i><a href="/input" style="color: #E41B17"> disini!!!</a></i></p>
      </div>
</div>
@endsection
