@extends('layout.dashboard')

@section('body')
<style>
    .bat{
        color: white;
        
    }
    .kumbang{
        color: black;
    }
</style>
  <div id="container" style="white-space:nowrap">
    <div id="image" style="display:inline;">
      <img src="../assets/img/profil.jpg" class="rounded" alt="ProfilPict" style="width: 160px; margin-top:50px">
    </div>
    <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
      <strong style="color: white;font-size:15px;margin-left:30px;">Hi,My name is Muhhamad Rayyan Rifqi Pratama, And i was born in Bandung 17 years ago</strong>
    </div>
</div>
<br><br><br>
  <div class="container-fluid" style="background: #73777B;align-items:center;"style="white-space:nowrap">
    <div class="row">
        <div id="tentang" style="display:inline;">
            <p class="bat" style="margin-right: 100px;margin-left:50px;margin-top:100px;margin-bottom:50px;font-size:30px">Apa itu <br><span style="color:#229bd8"><strong>Peduli diri</strong></span></p> 
         </div>
         <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
           <p class="bat" style="margin-top: 20px;margin-right:10px;margin-bottom:20px"><strong>
               PeduliDiri merupakan aplikasi catatan perjalanan yang dapat mencatat data <br> data perjalanan anda,Aplikasi ini dapat membantu tenaga kerja medis dikala pandemi<br> seperti sekarang. 
               <br><br>
            Aplikasi ini mengandalkan partisipasi masyarakat untuk saling membagikan data<br>lokasinya saat bepergian agar penelusuran riwayat kontak dengan penderita<br> COVID-19 dapat dilakukan.
        </strong></p>
         </div>   
    </div>
  </div>  
    <div class="container-fluid" style="background: #F1EEE9;margin-top:10px;margin-bottom:100px"style="white-space:nowrap">
    <div class="row">
        <div id="tentang" style="display:inline;">
            <p class="bat" style="margin-right: 25px;margin-left:50px;margin-top:100px;margin-bottom:50px;font-size:30px"><span style="color: black">Mengapa Harus</span><br><span style="color:#229bd8"><strong>Vaksinasi</strong></span></p> 
         </div>
         <div class="himb" id="texts" style="display:inline; white-space:nowrap;"> 
           <p class="kumbang" style="margin-top: 20px;margin-right:10px;margin-bottom:20px"><strong>Vaksin itu penting lho ternyata,karena dengan vaksin tubuh kita akan menjadi<br> kuat dari virus virus berbahaya diluar sana,karena vaksin dapat merangsang timbulnya <br>imun atau daya tahan tubuh seseorang. <br><br>
            Vaksin juga dapat mengurangi risiko penularan lho,kok bisa sih? karena ketika tubuh<br> kita disuntikkan oleh vaksin,vaksin tersebut akan merangsang antibodi untuk belajar<br> dan mengenali virus yang telah dilemahkan tersebut. Dengan demikian, tubuh akan <br>mengenali virus dan mengurang risiko terpapar.
        </strong></p>
         </div>   
    </div>
    </div>

@endsection