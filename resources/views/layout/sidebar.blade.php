
  <div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="#">Peduli Diri</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">PD</a>
      </div>
      <ul class="sidebar-menu">
         <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
              <a href="/home" class="btn btn-lg btn-block btn-icon-split" style="background: #EC994B">
                <i class="fas fa-home"></i>Home
              </a>
            </div>
        <li class="menu-header">Data</li>
          <li class="nav-item dropdown">
            <a href="#" class=" has-dropdown"><i class="fas fa-clipboard"></i><span>Data</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="/dashboard">Data User</a></li>
              <li><a class="nav-link" href="/input">Input</a></li>
            </ul>
          </li>
          <li class="menu-header">Logout</li>
          <li class="nav-item ">
            <a href="/logout" class="nav-link"> <i class="fas fa-sign-out-alt"></i> <span>Logout</span></a>
          </li>
      </ul>
    </aside>
  </div>
</div>
