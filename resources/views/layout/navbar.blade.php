<style>
.navbar{
  background: #EC994B;
}
</style>

<nav class="navbar navbar-expand-lg sticky-top">
  <div class="container">
    <div class="d-flex ">
      <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
      </ul>
      <a href="#" data-toggle="dropdown" class="nav-link  nav-link-lg nav-link-user">
        <img alt="image" src="../assets/img/avatar/avatar-6.png" class="rounded-circle mr-1">
        <div class="d-sm-none d-lg-inline-block ">HI,
           @if (!empty(auth()->user()->nama))
          {{ auth()->user()->nama }}   
          @else
           user 
          @endif</div></a>
    </div>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="d-flex flex-row-reverse">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home"><i class="fas fa-home"> Home</i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/about"><i class="fas fa-user">About</i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/logout"><i class="fas fa-sign-out"> 
              <div class="d-sm-none d-lg-inline-block "></div></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>



