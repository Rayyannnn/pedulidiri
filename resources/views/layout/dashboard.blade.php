@extends('layout.master')

@section('content')
      
    {{-- navbar --}}
    @include('layout.navbar')
                                                                                        
    {{-- sidebar --}}
    @include('layout.sidebar')


    <!-- Main Content -->
    <div class="main-content">
      <section class="section">
        
        @yield('body')

      </section>
    </div>

@endsection